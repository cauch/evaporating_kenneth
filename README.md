# Game:

Evaporating_Kenneth is a platformer game with a twist: each time you move,
you lose a bit of life.
The goal is to find the exit of the level, in the optimal way to avoid losing
all your health or getting stuck.

Few game play characteristics:
* You can jump, the height reached depends of your health level (so if you
miss a jump, it can mean that you have to restart the level because you don't
have enough health to make it anymore).
* You cannot direct yourself when jumping, you just conserve your momentum.
* When jumping, you lose as much health as when you move vertically.
* When you hit the ground after falling, you lose health in a quadratic way:
falling from x pixels makes you lose something proportional to x². It means
that you will lose less by falling from 3 times 10 pixels (~300) than by
falling 1 time 30 pixels (~900).

# Dependencies:

* Python 3.4.6 (http://www.python.org/)
* PyGame 1.9.3 (http://www.pygame.org/)

# Run the game:

python run_game.py  

# Licence:

Code:  
Evaporating_Kenneth is under GPL3 licence.  
Cf. COPYING

Font:
SIL Open Font License
https://www.fontsquirrel.com/fonts/bubblegum-sans
https://www.fontsquirrel.com/fonts/cagliostro

Graphics:  
created for the game and are under CC BY 3.0

Sounds:
cc Sampling Plus 1.0 and cc attrib 3.0, 
http://soundbible.com/1502-Slurping-2.html
http://soundbible.com/2067-Blop.html
http://soundbible.com/1139-Water-Drop-Low.html
http://soundbible.com/1463-Water-Balloon.html

Music:
Bansheebeat - Stargazer
(converted to ogg)
cc by-nc-sa 4.0
http://freemusicarchive.org/music/Bansheebeat/FrostClick_FrostWire_Creative_Commons_Mixtape_Vol_5_/12_Bansheebeat_Stargazer_Lumine_FROSTCLICK_FROSTWIRE_CREATIVE_COMMONS_MIXTAPE_VOL_5
