#! /usr/bin/env python3

#    This file is part of Evaporating_Kenneth.
#
#    Evaporating_Kenneth is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Evaporating_Kenneth is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Evaporating_Kenneth.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

import os
import random
import time

from game.ek_objects import *

class Menuitem(pygame.sprite.Sprite):
    def __init__(self,menu,options):
        """
        Sprite corresponding to an element of the menu
        """
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.menu = menu
        self.options = options
        self.isontop = 0
        if options.get("?") == "bkgd":
            #background of the panel
            x = options.get("x",111)
            dh = menu.display.screen_height
            dw = menu.display.screen_width
            h = options.get("h",dh-220)
            w = options.get("w",dw-110-45)
            y0 = dh*0.1
            y = options.get("y",y0+120)
            self.image = pygame.Surface((w,h)).convert_alpha()
            self.rect = self.image.get_rect()
            self.rect.left = x
            self.rect.top = y
            self.image.fill((255, 255, 255, 70))
        if options.get("?") == "text":
            self.cl = options.get("cl",water_color)
            self.text = options.get("text","")
            f = options.get("f","Ca18")
            self.image = menu.display.fonts[f].render( self.text, 1, self.cl)
            self.rect = self.image.get_rect()
            pos = options.get("p",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
        if options.get("?") in ["button","fakebutton"]:
            #button
            #added to self.menu.buttons
            self.cl = options.get("cl",water_color)
            self.text = options.get("text","")
            f = options.get("f","Ca22")
            text = menu.display.fonts[f].render( self.text, 1, self.cl)
            w = options.get("s",100)
            h = 35
            self.image = pygame.Surface((w+1,h+1)).convert_alpha()
            self.image.fill((0,0,0,0))
            #pygame.draw.line(self.image,self.cl,(0,0),(w,0))
            #pygame.draw.line(self.image,self.cl,(0,h),(w,h))
            #pygame.draw.line(self.image,self.cl,(0,0),(0,h))
            #pygame.draw.line(self.image,self.cl,(w,0),(w,h))
            #dx = (w-text.get_width())/2
            dx = 15
            self.image.blit(text, (dx,1))
            self.rect = self.image.get_rect()
            pos = options.get("p",[0,0])
            self.rect.left = int(round(pos[0]))
            self.rect.top = int(round(pos[1]))
            if options.get("B",0):
                fakecursor = pygame.Surface((w+1,h+1)).convert_alpha()
                fakecursor.fill((255,255,255,70))
                self.image.blit(fakecursor, (0,0))                
            if options.get("?") == "button":
                e = options.get("e",1)
                if e:
                    self.menu.buttons.append(self)
                self.value = options.get("value",self.text)
        if options.get("?") == "cursor":
            #cursor (black square)
            self.image = pygame.Surface((10,10)).convert_alpha()
            self.rect = self.image.get_rect()
            self.rect.left = -100
            self.rect.top = -100
            self.image.fill((0, 0, 0))
            self.value = -1

    def refresh(self):
        if self.isontop == 0:
            self.isontop = 1
            self.menu.menu_sprite.move_to_front(self)
            return 1
        return 1

class Menu:
    def __init__(self,display):
        self.display = display
        self.core = display.core

        self.bkgd = None
        self.bkgd_first = 0

        self.position = "main"

        self.menu_sprite = None
        self.menu_obj = []

        self.bkgd_sprite = None
        self.bkgd_obj = []

        self.buttons = []
        self.cursor = None

    def init(self):
 
        self.bkgd_sprite = pygame.sprite.LayeredUpdates()
        self.menu_sprite = pygame.sprite.LayeredUpdates()
        Player.containers = self.bkgd_sprite
        Drop.containers = self.bkgd_sprite
        Menuitem.containers = self.menu_sprite

        self.set_background()
        self.display.screen.blit(self.bkgd, (0,0))

        self.player_in_the_back()

        self.display_main()

    def player_in_the_back(self):
        self.player = Player(self)
        dh = self.display.screen_height
        dw = self.display.screen_width
        x1 = 250
        y1 = 0
        x2 = dw-50
        y2 = dh-140
        self.player_actions = [0,-1]
        self.player.pos = [(x1+x2)/2,(y1+y2)/2]
        self.player.size = 50
        self.pause_mode = "none"
        self.view_pos = [0,0]
        self.bonus = []
        self.walls = []
        self.walls.append(pygame.Rect([0,y1-100],[dw,100]))
        self.walls.append(pygame.Rect([0,y2],[dw,100]))
        #self.walls.append(pygame.Rect([int((x1+x2)/2),y2-60],[dw,100]))
        self.walls.append(pygame.Rect([x1-100,0],[100,dh]))
        self.walls.append(pygame.Rect([x2,0],[100,dh]))

        self.inside = pygame.Rect([x1,y1],[x2-x1,y2-y1])

        self.exit_properties = [0,0]
        self.bkgd_obj.append(self.player)

    def create_drop(self,pos,v=None):
        d1 = Drop(self,pos)
        d1.size = 3
        if v is None:
            d1.vx = 0.1*(10-random.randint(0,20))
            d1.vy = 0.5*(10-random.randint(0,20))
        else:
            d1.vx = v[0]
            d1.vy = v[1]
        d2 = Drop(self,pos[:])
        d2.following = d1
        self.bkgd_obj.append(d1)
        self.bkgd_obj.append(d2)

    def set_background(self):
        if self.bkgd:
            return
        rw,rh = self.display.screen_width, self.display.screen_height
        self.bkgd = pygame.Surface((rw,rh))
        self.bkgd.fill( (30,30,30) )

    def check_key(self,key):
        pass

    def check_events(self,event):
        if event.type == ACTIVEEVENT:
            pass
        elif event.type == KEYDOWN:
            key = event.key
            if key == K_UP or key == K_LEFT or key == K_z or key == K_w:
                self.move_cursor(-1)
            elif key == K_DOWN or key == K_RIGHT or key == K_s:
                self.move_cursor(1)
            elif event.key == K_SPACE or event.key == K_RETURN:
                    if self.cursor and self.cursor.value != -1:
                        self.click_on_button(self.cursor.value)

    def click_on_button(self,i,silent=0):
        if silent == 0:
            self.display.play_sound("blop",0.5)
        value = self.buttons[i].value
        if value == "quit":
            self.display.core.play = 0
        elif value == "play":
            self.display_clean()
            self.display_play()
            #self.core.new_game()
        elif value.startswith("pla_"):
            level = value[4:]
            self.core.new_game(level)
        elif value.startswith("plaB_"):
            self.display_clean()
            self.display_play(int(value.split("_")[1]))
        elif value == "about":
            self.display_clean()
            self.display_about()
        elif value == "scoresT":
            self.display_clean()
            self.display_scoresT()
        elif value.startswith("scoT_"):
            self.display_clean()
            self.display_scoresT(int(value.split("_")[1]))
        elif value == "scores":
            self.display_clean()
            self.display_scoresS()
        elif value.startswith("scoS_"):
            self.display_clean()
            self.display_scoresS(int(value.split("_")[1]))
        elif value == "help":
            self.display_clean()
            self.display_help()
        elif value == "options":
            self.display_clean()
            self.display_options()
        elif value.startswith("optB_"):
            self.display_clean()
            self.display_options(value.split("_")[1])
        elif value == "back":
            self.display_clean()
            self.display_main()
        else:
            print("TODO",value)

    def check_player_actions(self):
        if not self.inside.colliderect(self.player.rect):
            self.player.pos = [self.inside.centerx,self.inside.centery]
            self.player.momentum = [0,0]
        self.player_actions[0] += -1
        if self.player_actions[0] < 0:
            self.player_actions[0] = random.randint(10,20)
            if self.player_actions[1] == 0:
                self.player.move(0,-7)
            elif self.player_actions[1] == 1:
                self.player.move(-2,0)
            elif self.player_actions[1] == 2:
                self.player.move(2,0)
            self.player_actions[1] = random.randint(0,4)

    def update(self):

        list_layers = [self.bkgd_sprite,
                       self.menu_sprite,
        ]
        list_objects = [self.bkgd_obj,
                        self.menu_obj,
        ]

        tokill = []
        self.player.size = 50
        self.check_player_actions()            

        for i in range(len(list_layers)):
            for o in list_objects[i][:]:
                r = o.refresh()
                if r==-1:
                    tokill.append(o)
                    list_objects[i].remove(o)

        dirty = []
        for i in range(len(list_layers)):
            if list_layers[i]:
                list_layers[i].clear(self.display.screen, self.bkgd)
        for i in range(len(list_layers)):
            if list_layers[i]:
                dirty += list_layers[i].draw(self.display.screen)
        
        for o in tokill:
            o.kill()

        redraw = 0
        if self.bkgd_first == 0:
            self.bkgd_first = 1
            redraw = 1

        return dirty,redraw

    def set_cursor(self,i,silent=0):
        if self.cursor.value == i:
            #already in place
            return
        self.cursor.value = i
        if i >= len(self.buttons):
            #no button for this index
            self.cursor.rect.left = -100
            return
        if silent == 0:
            self.display.play_sound("blop",0.5)
        #place the sprite

        w,h = self.buttons[i].rect.width,self.buttons[i].rect.height
        self.cursor.image = pygame.Surface((w,h)).convert_alpha()
        self.cursor.rect = self.cursor.image.get_rect()
        self.cursor.rect.left = self.buttons[i].rect.left
        self.cursor.rect.top = self.buttons[i].rect.top
        self.cursor.image.fill((255, 255, 255, 70))
        cl = (255,255,255)
        pygame.draw.line(self.cursor.image,cl,(0,0),(w-1,0))
        pygame.draw.line(self.cursor.image,cl,(0,h-1),(w-1,h-1))
        pygame.draw.line(self.cursor.image,cl,(0,0),(0,h-1))
        pygame.draw.line(self.cursor.image,cl,(w-1,0),(w-1,h-1))

        #self.cursor.rect.left = self.buttons[i].rect.left+6
        #self.cursor.rect.top = self.buttons[i].rect.top+(self.buttons[i].rect.height/2)-(self.cursor.rect.height/2)

    def move_cursor(self,i):
        # i: 1 going down, -1 going up
        new_value = self.cursor.value + i
        if new_value < 0:
            new_value = len(self.buttons)-1
        if new_value >= len(self.buttons):
            new_value = 0
        self.set_cursor(new_value)

    def display_clean(self):
        otk = []
        for o in self.menu_obj:
            if o.__class__.__name__ == "Menuitem":
                otk.append(o)
                o.kill()
        for o in otk:
            self.menu_obj.remove(o)
        self.buttons = []

    def display_main(self,fake=None):
        a = self.menu_obj.append

        dh = self.display.screen_height
        dw = self.display.screen_width
        x,y = dw*0.2,dh*0.1

        type_ = "button"
        if fake is not None:
            type_ = "fakebutton"

        a(Menuitem(self,
                   {"?":"text","text":"Evaporating","p":[x+100,y],"f":"title"}))
        a(Menuitem(self,
                   {"?":"text","text":"Kenneth","p":[x+250,y+50],"f":"title"}))
        y += 120
        x = 10
        texts = ["play","scores","options","help","about","quit"]
        for te in texts:
            a(Menuitem(self,
                       {"?":type_,"text":te,"p":[x,y],"s":100,"B":(te==fake)}))
            y += 50

        if fake is None:
            self.cursor = Menuitem(self,{"?":"cursor"})
            a(self.cursor)
            self.set_cursor(0,silent=1)

    def display_play(self,page=0):
        self.display_main("play")
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","h":260}))
        x0 = self.menu_obj[-1].rect.left+10
        y0 = self.menu_obj[-1].rect.top
        x = x0+10
        y = y0+10

        old = [_.split()[0] for _ in self.core.profile.scoresbs]
        current = self.core.profile.lastlevel
        old.sort()
        old += ["allcutscene"]
        if "level01" not in old:
            old = ["level01"]+old
        if "cutscene" not in current and not current.startswith("!"):
            levels = [current]+old
        else:
            levels = old
        cpn1 = levels[page*6:]
        cpn2 = cpn1[:6]
        oy = y
        for l in cpn2:
            a(Menuitem(self,
                       {"?":"button","text":l,"p":[x,y],"value":"pla_"+l,"s":200}))
            y += 40
        y = oy+(6*40)+20
        if page > 0:
            a(Menuitem(self,
                       {"?":"button","text":"<","value":"plaB_%i"%(page-1),"p":[x,y],"s":40}))            
        if len(cpn1)>6:
            a(Menuitem(self,
                       {"?":"button","text":">","value":"plaB_%i"%(page+1),"p":[x+60,y],"s":40}))
        a(Menuitem(self,
                   {"?":"button","text":"back","p":[x+120,y],"s":80}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)

    def display_scoresS(self,page=0):
        self.display_main("scores")
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","h":260}))
        x0 = self.menu_obj[-1].rect.left+10
        y0 = self.menu_obj[-1].rect.top
        x = x0+10
        y = y0+10

        score = self.core.profile.scoresbs[:]
        score.sort()
        cpn1 = score[page*8:]
        cpn2 = cpn1[:8]
        oy = y
        for l in cpn2:
            t1 = l.split()[0]
            t2 = l.split()[1]+" "+l.split()[2]
            t3 = l.split()[3]
            a(Menuitem(self,
                       {"?":"text","text":t1,"p":[x,y],"f":"Ca18"}))
            a(Menuitem(self,
                       {"?":"text","text":t2,"p":[x+170,y],"f":"Ca18"}))
            a(Menuitem(self,
                       {"?":"text","text":t3,"p":[x+310,y],"f":"Ca18"}))
            y += 30
        y = oy+(8*30)+20
        if page > 0:
            a(Menuitem(self,
                       {"?":"button","text":"<","value":"scoS_%i"%(page-1),"p":[x,y],"s":40}))            
        if len(cpn1)>8:
            a(Menuitem(self,
                       {"?":"button","text":">","value":"scoS_%i"%(page+1),"p":[x+60,y],"s":40}))
        a(Menuitem(self,
                   {"?":"button","text":"time","value":"scoresT","p":[x+120,y],"s":80}))
        a(Menuitem(self,
                   {"?":"button","text":"back","p":[x+220,y],"s":80}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)

    def display_scoresT(self,page=0):
        self.display_main("scores")
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","h":260}))
        x0 = self.menu_obj[-1].rect.left+10
        y0 = self.menu_obj[-1].rect.top
        x = x0+10
        y = y0+10

        score = self.core.profile.scoresbt[:]
        score.sort()
        cpn1 = score[page*8:]
        cpn2 = cpn1[:8]
        oy = y
        for l in cpn2:
            t1 = l.split()[0]
            t2 = l.split()[1]+" "+l.split()[2]
            t3 = int(l.split()[3])
            if t3 > 60*60:
                t3 = time.strftime('%H:%M:%S', time.gmtime(t3))
            else:
                t3 = time.strftime('%M:%S', time.gmtime(t3))
            a(Menuitem(self,
                       {"?":"text","text":t1,"p":[x,y],"f":"Ca18"}))
            a(Menuitem(self,
                       {"?":"text","text":t2,"p":[x+170,y],"f":"Ca18"}))
            a(Menuitem(self,
                       {"?":"text","text":t3,"p":[x+310,y],"f":"Ca18"}))
            y += 30
        y = oy+(8*30)+20
        if page > 0:
            a(Menuitem(self,
                       {"?":"button","text":"<","value":"scoT_%i"%(page-1),"p":[x,y],"s":40}))            
        if len(cpn1)>8:
            a(Menuitem(self,
                       {"?":"button","text":">","value":"scoT_%i"%(page+1),"p":[x+60,y],"s":40}))
        a(Menuitem(self,
                   {"?":"button","text":"point","value":"scores","p":[x+120,y],"s":80}))
        a(Menuitem(self,
                   {"?":"button","text":"back","p":[x+220,y],"s":80}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)


    def display_options(self,change=""):
        self.display_main("options")
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","h":200}))
        x0 = self.menu_obj[-1].rect.left+10
        y0 = self.menu_obj[-1].rect.top
        x = x0+10
        y = y0+10

        cp = 0
        if change == "s1":
            self.core.profile.sound1 = (self.core.profile.sound1+1)%6
            self.display.change_volume_music()
        if change == "s2":
            cp = 1
            self.core.profile.sound2 = (self.core.profile.sound2+1)%6
        if change == "fs":
            cp = 2
            self.core.profile.fullscreen = (self.core.profile.fullscreen+1)%2
            self.display.set_screen(self.core.profile.fullscreen)
            self.display.redraw()
        if change == "re":
            self.core.profile.reset()

        s1 = self.core.profile.sound1
        s2 = self.core.profile.sound2
        music_text = (s1*"#")+((5-s1)*"_")
        sound_text = (s2*"#")+((5-s2)*"_")

        a(Menuitem(self,
                   {"?":"button","text":"music: [%s]"%(music_text),"value":"optB_s1","p":[x,y],"s":200}))
        y += 40
        a(Menuitem(self,
                   {"?":"button","text":"sound: [%s]"%(sound_text),"value":"optB_s2","p":[x,y],"s":200}))
        y += 40
        if self.core.profile.fullscreen:
            a(Menuitem(self,
                       {"?":"button","text":"fullscreen ON","value":"optB_fs","p":[x,y],"s":200}))
        else:
            a(Menuitem(self,
                       {"?":"button","text":"fullscreen OFF","value":"optB_fs","p":[x,y],"s":200}))
        y += 40
        #a(Menuitem(self,
        #           {"?":"button","text":"restart from level 1","value":"optB_re","p":[x,y],"s":200}))

        if change:
            self.core.profile.save()

        y = y0+210
        a(Menuitem(self,
                   {"?":"button","text":"back","B":1,"p":[x,y],"s":100}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(cp,silent=1)

    def display_about(self):
        self.display_main("about")
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","h":260}))
        x0 = self.menu_obj[-1].rect.left+10
        y0 = self.menu_obj[-1].rect.top
        x = x0+10
        y = y0+10
        lot = ["Created for pyweek26",
               "https://pyweek.org",
               "October 2018","",
               "by Cauch","",
               "GNU GPL3 licence, sources available here:",
               "https://gitlab.com/cauch/evaporating_kenneth"]
        for l in lot:
            if l:
                a(Menuitem(self,
                           {"?":"text","text":l,"p":[x,y],"f":"Ca22"}))
                y += 30
            else:
                y += 20
        y = y0+270
        a(Menuitem(self,
                   {"?":"button","text":"back","p":[x,y],"s":100}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)


    def display_help(self):
        self.display_main("help")
        a = self.menu_obj.append
        a(Menuitem(self,
                   {"?":"bkgd","h":340}))
        x0 = self.menu_obj[-1].rect.left+10
        y0 = self.menu_obj[-1].rect.top
        x = x0+10
        y = y0+10
        lot = ["Use the left-right arrow keys to move horizontally",
               "Use the up arrow key to jump",
               "Use the down arrow to shrink",
               "Try to find the exit to finish the level",
               "It is possible to be blocked and forced to restart the level","",
               "You lose health when you move horizontally or hit the floor",
               "You can gain health using bonuses",
               "You die if you have no health or too much health","",
               "You cannot change direction while in the middle of a jump"
           ]
        for l in lot:
            if l:
                a(Menuitem(self,
                           {"?":"text","text":l,"p":[x,y],"f":"Ca22"}))
                y += 30
            else:
                y += 20
        y = y0+350
        a(Menuitem(self,
                   {"?":"button","text":"back","p":[x,y],"s":100}))
        self.cursor = Menuitem(self,{"?":"cursor"})
        a(self.cursor)
        self.set_cursor(0,silent=1)

    def end(self):
        self.bkgd = None
        self.bkgd_first = 0

        self.position = "main"

        self.menu_sprite = None
        self.menu_obj = []
        self.bkgd_sprite = None
        self.bkgd_obj = []

        self.player = None

        self.buttons = []
        self.cursor = None

