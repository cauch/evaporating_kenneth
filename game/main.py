#    This file is part of Evaporating Kenneth.
#
#    Evaporating Kenneth is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Evaporating Kenneth is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Evaporating Kenneth.  If not, see <http://www.gnu.org/licenses/>.

import game.ek_core as ek_core
import game.ek_display as ek_display

def main():
    core = ek_core.Core()
    display = ek_display.Display()
    core.display = display
    display.core = core
    core.load()

