#! /usr/bin/env python3

#    This file is part of Evaporating_Kenneth.
#
#    Evaporating_Kenneth is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Evaporating_Kenneth is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Evaporating_Kenneth.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

import os
import random
import math
import time

import game.ek_core as ek_core
import game.ek_display as ek_display

class Decor(pygame.sprite.Sprite):
    def __init__(self,core,keyname,pos):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.core = core
        self.display = core.display
        self.play = core.display.module["creator"]
        self.keyname = keyname
        self.pos = pos[:]
        if self.keyname == "!wall":
            if len(pos) == 2:
                self.image = pygame.Surface((40,40)).convert_alpha()
                self.image.fill((0,0,0,0))
                pygame.draw.circle(self.image, (100,0,0), [20,20], 10)
            else:
                w,h = int(pos[2]/2),int(pos[3]/2)
                x1,y1 = int(self.pos[0]/2),int(self.pos[1]/2)
                self.image = pygame.Surface((w,h)).convert_alpha()
                self.image.fill((100,0,0,100))
        else:
            self.image = self.display.images[keyname]
        self.rect = self.image.get_rect()
        self.nr = 1
        self.ignore_vp = 0
        self.refresh()
 
    def refresh(self,force_refresh=0):
        if self.nr == 0 and force_refresh == 0:
            return 0
        self.nr = 0
        #self.image = self.display.images[self.keyname]
        self.rect = self.image.get_rect()
        vp = self.play.view_pos
        if self.ignore_vp:
            vp = [0,0]
        self.rect.left = int( (round(self.pos[0])+vp[0]) /2)
        self.rect.top = int( (round(self.pos[1])+vp[1]) /2)
        return 1

class Creator:
    def __init__(self,display):
        self.display = display
        self.core = display.core

        self.pause_mode = "none"
        self.wait = 0

        self.filename = None

        self.time = [None,0]

        self.bkgd = None
        self.bkgd_first = 0 #0 if the bkgd should be redrawn
        
        self.layer1_obj = []
        self.layer1_sprite = None

        self.loaded = {}
        self.loaded["walls"] = []
        self.loaded["decor_bkgd"] = []
        self.loaded["decor_frgd"] = []

        self.last_key_time = 0

        self.wall_click = None

        self.selection = ["!wall"]
        self.view_pos = [0,0]

    def init(self):        
        self.layer1_sprite = pygame.sprite.LayeredUpdates()
        Decor.containers = self.layer1_sprite
        self.bkgd = None
        self.bkgd_first = 0 #0 if the bkgd should be redrawn

        for k in self.display.images:
            im = self.display.images[k]
            w,h = im.get_rect().width,im.get_rect().height
            im2 = pygame.transform.scale(im,(int(w/2),int(h/2)))
            self.display.images[k] = im2
            print(w,h)

        keys = []
        for k in self.display.images.keys():
            if k.startswith("player_"):
                continue
            if k.startswith("drop_"):
                continue
            if k in ["icon"]:
                continue
            keys.append(k)
        keys.sort()
        self.selection += keys 

        #for o in self.core.loaded.get("decor_bkgd",[]):
        #    d = DecorBkgd(self.core,o[2],o[:2])
        #    self.layer0_obj.append(d)

        #for o in self.core.loaded.get("walls",[]):
        #    w = pygame.Rect(o)
        #    self.walls.append(w)

        #self.player = Player(self.core)
        #for k in self.core.loaded.get("player_prop",{}):
        #    va = self.core.loaded["player_prop"][k]
        #    setattr(self.player,k,va)
        #self.player.nr = 1
        #self.layer1_obj.append(self.player)
        self.recreate()
        if self.filename:
            self.loadlevel(self.filename)

    def set_background(self):
        if self.bkgd:
            return
        bg = []
        rw,rh = self.display.screen_width, self.display.screen_height
        self.bkgd = pygame.Surface((rw,rh))
        cl = (0,0,0)
        self.bkgd.fill( cl )
        for i in range(0,441,10):
            pygame.draw.line(self.bkgd, (255,255,255), [0,i], [1000,i])
        for i in range(0,800,10):
            pygame.draw.line(self.bkgd, (255,255,255), [i,0], [i,440])
        pygame.draw.line(self.bkgd, (255,255,255), [20,440], [20,600])
        pygame.draw.line(self.bkgd, (255,255,255), [0,520], [20,520])
        pygame.draw.line(self.bkgd, (255,0,0), [20,440], [60,440])
        pygame.draw.line(self.bkgd, (255,0,0), [60,440], [60,600])

    def recreate(self):
        for o in self.layer1_obj[:]:
            self.layer1_obj.remove(o)
            o.kill()
        x = 40
        for s in self.selection:
            y = random.randint(0,80)
            o = Decor(self.core,s,[x,880+y])
            x += 80
            o.nr = 1
            o.ignore_vp = 1
            self.layer1_obj.append(o)

        for i in self.loaded.get("decor_bkgd",[]):
            x,y,k = i
            o = Decor(self.core,k,[x,y])
            o.nr = 1
            self.layer1_obj.append(o)
        for i in self.loaded.get("walls",[]):
            x1,y1,x2,y2 = i
            o = Decor(self.core,"!wall",[x1,y1,x2,y2])
            o.nr = 1
            self.layer1_obj.append(o)

    def savelevel(self):
        t = []
        t.append('self.loaded["background_cl"] = [30,30,30]')
        t.append('self.loaded["decor_bkgd"] = []')
        t.append('self.loaded["walls"] = []')
        t.append('self.loaded["bonus"] = []')

        for s in self.loaded.get("decor_bkgd",[]):
            if s[2] == "drop":
                t.append('self.loaded["bonus"].append([%i,%i])'%(s[0],s[1]))
            else:
                t.append('self.loaded["decor_bkgd"].append([%i,%i,"%s"])'%(s[0],s[1],s[2]))
        for s in self.loaded.get("walls",[]):
            t.append('self.loaded["walls"].append([%i,%i,%i,%i])'%(s[0],s[1],s[2],s[3]))

        t.append('self.loaded["player_prop"] = '+repr(self.loaded.get("player_prop",{"pos":[80,160],"size":50,"ori":"l","parts":[]})))
        t.append('self.loaded["exit"] = '+repr(self.loaded.get("exit",[0,0,"nextlevel"])))
        t.append('self.loaded["view_pos"] = '+repr(self.loaded.get("view_pos",[0,0])))
        t.append('self.loaded["intro_message"] = '+repr(self.loaded.get("intro_message",[])))

        level_name = self.filename
        if not level_name:
            level_name = "unnamed"
        level_name += "_wip"
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","level","%s.py"%level_name)
        f = open(filename,"w")
        for tt in t:
            f.write(tt+"\n")
            print(tt)
        f.close()
        print("save over",filename)

    def loadlevel(self,level_name):
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","level","%s.py"%level_name)
        print("opening",filename)
        if os.path.exists(filename):
            exec(open(filename).read())
        t = self.loaded.get("bonus",[])
        self.loaded["decor_bkgd"] += [_+["drop"] for _ in t]
        self.recreate()
            
    def check_key(self,key):
        if abs(self.last_key_time - time.time()) < 0.6:
            return
        self.last_key_time = time.time()
        if self.pause_mode == "none":
            if key == K_z:
                if self.selection[0] == "!wall":
                    pass
                else:
                    self.loaded["decor_bkgd"].pop()
                self.recreate()
            elif key == K_s:
                self.savelevel()
            elif key == K_UP:
                self.view_pos[1] += 200
                self.recreate()
            elif key == K_DOWN:
                self.view_pos[1] += -200
                self.recreate()
            elif key == K_LEFT:
                self.view_pos[0] += 200
                self.recreate()
            elif key == K_RIGHT:
                self.view_pos[0] += -200
                self.recreate()
            # if key == K_UP or key == K_z or key == K_w:
            #     #self.player.size += -1
            #     self.player.move_key(0,-10)
            # elif key == K_DOWN or key == K_s:
            #     self.player.size += -1
            #     if self.player.size == 19:
            #         self.player.size = 80
            #     self.player.move_key(0,0)
            # elif key == K_LEFT or key == K_a or key == K_q:
            #     self.player.move_key(-0.1,0)
            # elif key == K_RIGHT or key == K_d:
            #     self.player.move_key(0.1,0)

    def mousepress(self,pressed,pos):
        if pressed[2] == 1:
            x,y = (pos[0]*2)-self.view_pos[0],(pos[1]*2)-self.view_pos[1]
            x,y = int(x/20),int(y/20)

            for _ in self.loaded["walls"][:]:
                if [x*20,y*20] == _[:2]:
                    self.loaded["walls"].remove(_)
            for _ in self.loaded["decor_bkgd"][:]:
                if [x*20,y*20] == _[:2]:
                    self.loaded["decor_bkgd"].remove(_)
            self.recreate()
            return
        if pressed[0] == 0:
            return
        if pos[1] > 440:
            self.wall_click = None
            if pos[0] < 20 and pos[1] < 520:
                p = self.selection.pop(0)
                self.selection.append(p)
            if pos[0] < 20 and pos[1] > 520:
                p = self.selection.pop()
                self.selection = [p]+self.selection
            else:
                pi = int((pos[0]-20)/40)
                if pi < len(self.selection):
                    self.selection = self.selection[pi:]+self.selection[:pi]
            self.recreate()
        else:
            x,y = (pos[0]*2)-self.view_pos[0],(pos[1]*2)-self.view_pos[1]
            x,y = int(x/20),int(y/20)
            if self.selection[0] == "!wall":
                if self.wall_click is None:
                    self.wall_click = [x,y]
                else:
                    xr1,yr1 = self.wall_click
                    xr2,yr2 = x,y
                    x1,x2 = min(xr1,xr2),max(xr1,xr2)
                    y1,y2 = min(yr1,yr2),max(yr1,yr2)
                    x2,y2 = x2+1,y2+1
                    w,h = abs(x1-x2),abs(y1-y2)
                    self.loaded["walls"].append([x1*20,y1*20,w*20,h*20])
                    self.wall_click = None
                    o = Decor(self.core,"!wall",[x1*20,y1*20,w*20,h*20])
                    o.nr = 1
                    self.layer1_obj.append(o)
            else:
                self.wall_click = None
                keyname = self.selection[0]
                o = Decor(self.core,keyname,[x*20,y*20])
                o.nr = 1
                self.layer1_obj.append(o)
                self.loaded["decor_bkgd"].append([x*20,y*20,keyname])

    def check_events(self,event):
        if event.type == ACTIVEEVENT:
            ##turn on pause if the window is losing focus
            #if self.core.config.pausewhenout:
            #    if event.state == 2 and event.gain == 0:
            #        self.mode = "pause"
            pass
        elif event.type == MOUSEBUTTONDOWN:
            self.mousepress(pygame.mouse.get_pressed(),pygame.mouse.get_pos()) 
        elif event.type == KEYDOWN:
            key = event.key
            if self.pause_mode == "none":
                if key == K_ESCAPE or key == K_p:
                    self.set_pause()
                elif key == K_r:
                    self.reload_level()
            elif self.pause_mode == "pause":
                if key == K_p:
                    self.pause_mode = "none"
                if key == K_ESCAPE:
                    self.pause_mode = "none"
                    self.display.mode = "menu"
                    self.core.back_to_menu()

    def reload_level(self):
        pass

    def set_pause(self):
        ct = self.time[0]
        st = self.time[1]
        dt = time.time()-ct
        self.time[0] = None
        self.time[1] = st+dt
        self.pause_mode = "pause"
        #t = "PAUSE\n\n"
        #t += "press 'p' to continue\n"
        #t += "press 'ESC' to abandon the game and exit to menu"
        #t += "\n\n"+self.core.loaded.get("intro","")
        #self.add_curtain(t)


    def update(self):
        dirty = []

        if self.pause_mode == "none":
            if self.time[0] == None:
                self.time[0] = time.time()

        #if self.layer1_sprite == None:
        #    return [],0

        if self.bkgd == None:
            self.set_background()
            self.display.screen.blit(self.bkgd, (0,0))


        list_layers = [
                       self.layer1_sprite,
        ]
        list_objects = [
                        self.layer1_obj,
        ]

        tokill = []
        force_refresh = 0

        for i in range(len(list_layers)):
            for o in list_objects[i][:]:
                r = o.refresh(force_refresh)
                if r==-1:
                    tokill.append(o)
                    list_objects[i].remove(o)

        dirty = []
        for i in range(len(list_layers)):
            if list_layers[i]:
                list_layers[i].clear(self.display.screen, self.bkgd)
        for i in range(len(list_layers)):
            if list_layers[i]:
                dirty += list_layers[i].draw(self.display.screen)
        
        for o in tokill:
            o.kill()

        if self.wait > 0:
            self.wait += -1
            return [],1

        redraw = 0
        if self.bkgd_first == 0:
            self.bkgd_first = 1
            redraw = 1

        return dirty,redraw

    def get_time(self):
        dt = 0
        if self.time[0] != None:
            dt = time.time()-self.time[0]
        tt = dt+self.time[1]
        return tt

    def end(self):
        for o in self.layer1_obj:
            o.kill()
        for o in self.layer2_obj:
            o.kill()

        self.bkgd = None
        self.bkgd_first = 0

        self.time = [None,0]

        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None
        
def main():
    core = ek_core.Core()
    display = ek_display.Display()
    core.display = display
    display.core = core
    display.module["creator"] = Creator(display)
    display.mode = "creator"
    import sys
    if len(sys.argv)>2:
        display.module["creator"].filename = sys.argv[2]
    core.load()
