#! /usr/bin/env python

#    This file is part of Evaporating_Kenneth.
#
#    Evaporating_Kenneth is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Evaporating_Kenneth is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Evaporating_Kenneth.  If not, see <http://www.gnu.org/licenses/>.

import os
import datetime

class Profile:
    def __init__(self,core):
        self.core = core
        self.display = core.display

        #options
        self.sound1 = 2
        self.sound2 = 1
        self.fullscreen = 0
        self.scoresbt = []
        self.scoresbs = []
        self.lastlevel = "cutscene01"

    def reset(self):
        self.lastlevel = "cutscene01"
        self.save()

    def load(self):
        filename= os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               "..","save.cfg")
        if not os.path.exists(filename):
            n = self.save()
        fi = open(filename,"r")
        da = fi.readlines()
        self.scoresbt = []
        self.scoresbs = []
        for d in da:
            t = d.strip()
            if t.startswith("sound1"):
                self.sound1 = int(t.split()[1])
            if t.startswith("sound2"):
                self.sound2 = int(t.split()[1])
            if t.startswith("fullscreen"):
                self.fullscreen = int(t.split()[1])
            if t.startswith("scorebs"):
                self.scoresbs.append(" ".join(t.split()[1:]))
            if t.startswith("scorebt"):
                self.scoresbt.append(" ".join(t.split()[1:]))
            if t.startswith("lastlevel"):
                self.lastlevel = " ".join(t.split()[1:])

    def change(self):
        self.save()
        self.core.display.change_volume_music()

    def save(self):
        filename = "save.cfg"
        tfilename= os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                "..",filename)
        fi = open(tfilename,"w")
        fi.write("sound1 %i\n"%self.sound1)
        fi.write("sound2 %i\n"%self.sound2)
        fi.write("fullscreen %i\n"%self.fullscreen)
        fi.write("lastlevel %s\n"%self.lastlevel)        
        for s in self.scoresbs:
            fi.write("scorebs %s\n"%s)
        for s in self.scoresbt:
            fi.write("scorebt %s\n"%s)
        fi.close()

    def add_highscore(self,level_name,point=0,playtime=0):
        addedbs = 0
        addedbt = 0
        time = datetime.datetime.now().strftime("%d/%m/%y %H:%M")
        textbs = "%s %s %i"%(level_name,time,point)
        textbt = "%s %s %i"%(level_name,time,playtime)
        for i in range(len(self.scoresbs)):
            if level_name == self.scoresbs[i].split()[0]:
                addedbs = 1
                os = int(self.scoresbs[i].split()[3])
                if point > os:
                    addedbs = 2
                    self.scoresbs[i] = textbs
        if addedbs == 0:
            self.scoresbs.append(textbs)
            addedbs = 2
        for i in range(len(self.scoresbt)):
            if level_name == self.scoresbt[i].split()[0]:
                addedbt = 1
                os = int(self.scoresbt[i].split()[3])
                if playtime < os:
                    addedbt = 2
                    self.scoresbt[i] = textbt
        if addedbt == 0:
            self.scoresbt.append(textbt)
            addedbt = 2
        if addedbs == 2 or addedbt == 2:
            self.save()
