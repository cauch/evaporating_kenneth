#! /usr/bin/env python3

#    This file is part of Evaporating_Kenneth.
#
#    Evaporating_Kenneth is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Evaporating_Kenneth is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Evaporating_Kenneth.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

import os
import random
import math
import time

from game.ek_objects import *

class Bonus(pygame.sprite.Sprite):
    def __init__(self,play,pos):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.play = play
        self.core = play.core
        self.display = play.display
        self.image = self.display.images["drop"]
        self.rect = self.image.get_rect()
        self.pos = pos
        self.nr = 1
        self.refresh()
 
    def refresh(self,force_refresh=0):
        if self.nr == 0 and force_refresh == 0:
            return 0
        if self.nr == -1:
            return -1
        self.nr = 0
        w,h = self.rect.width,self.rect.height
        self.rect.left = int(round(self.pos[0])+self.play.view_pos[0])
        self.rect.top = int(round(self.pos[1])+self.play.view_pos[1])
        return 1

class Decor(pygame.sprite.Sprite):
    def __init__(self,play,keyname,pos):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.play = play
        self.display = play.display
        self.core = play.core
        self.keyname = keyname
        if keyname.startswith("!"):
            self.image = pygame.Surface((1000,1000))
            if keyname == "!sky":
                self.image.fill((0,0,200))
            if keyname == "!sea":
                self.image.fill(water_color)
        else:
            self.image = self.display.images[keyname]
        self.rect = self.image.get_rect()
        self.pos = pos
        self.nr = 1
        self.refresh()
 
    def refresh(self,force_refresh=0):
        if self.nr == 0 and force_refresh == 0:
            return 0
        self.nr = 0
        if self.keyname.startswith("!"):
            self.image = pygame.Surface((1000,1000))
            if self.keyname == "!sky":
                self.image.fill((50,120,180))
            if self.keyname == "!sea":
                self.image.fill(water_color)
        else:
            self.image = self.display.images[self.keyname]
        self.rect = self.image.get_rect()
        self.rect.left = int(round(self.pos[0])+self.play.view_pos[0])
        self.rect.top = int(round(self.pos[1])+self.play.view_pos[1])
        return 1

class DecorBkgd(Decor):
    def __init__(self,play,keyname,pos):
        Decor.__init__(self,play,keyname,pos)
class DecorFrgd(Decor):
    def __init__(self,play,keyname,pos):
        Decor.__init__(self,play,keyname,pos)
class SpecialBE(DecorBkgd):
    def __init__(self,play,pos,lifetime,wall):
        self.lifetime = lifetime
        self.wall = wall
        Decor.__init__(self,play,"girder1",pos)

    def refresh(self,force_refresh=0):
        self.lifetime += -1
        if self.nr == 0 and force_refresh == 0 and self.lifetime != 0:
            return 0
        self.nr = 0
        if self.lifetime == 0:
            if self.wall is not None and self.wall in self.play.walls:
                self.play.walls.remove(self.wall)
            self.keyname = "bulletexplosion"
            self.pos[0] += -9
            self.pos[1] += -49
        self.image = self.display.images[self.keyname]
        self.rect = self.image.get_rect()
        self.rect.left = int(round(self.pos[0])+self.play.view_pos[0])
        self.rect.top = int(round(self.pos[1])+self.play.view_pos[1])
        return 1
        

class Gauge(pygame.sprite.Sprite):
    def __init__(self,play):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.play = play
        self.display = play.display
        self.core = play.core
        self.image = pygame.Surface((20,120)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.rect = self.image.get_rect()
        self.rect.left = 10
        self.rect.top = self.display.screen_height-120-10
        self.bubbles = []
        self.refresh()
 
    def refresh(self,force_refresh=0):
        if self.play.pause_mode != "none":
            return 0
        y = int((80-self.play.player.size)*114/60.)
        self.image.fill((0,0,0,0))
        pygame.draw.rect(self.image, (0,0,0), pygame.Rect(0,0,20,110))
        pygame.draw.circle(self.image, (0,0,0), (10,110), 10)
        image2 = pygame.Surface((14,114)).convert_alpha()
        image2.fill((0,0,0,0))
        pygame.draw.rect(image2, water_color, pygame.Rect(0,0,14,107))
        pygame.draw.circle(image2, water_color, (7,107), 7)
        water_color2 = [min(255,_+20) for _ in water_color]
        for b in self.bubbles[:]:
            bx,by = b[0],b[1]
            if bx > 0 and random.randint(0,15)==0:
                bx += -1
            elif bx < 13 and random.randint(0,15)==0:
                bx += 1
            if by < y:
                self.bubbles.remove(b)
            b[1] += -1
            pygame.draw.rect(image2, water_color2, pygame.Rect(bx,by,2,2))
        if random.randint(0,20) == 0:
            self.bubbles.append([random.randint(0,13),random.randint(108-y,110)])
        pygame.draw.rect(image2, (0,0,0,0), pygame.Rect(0,0,14,y))
        self.image.blit(image2, (3,3))
        return 1

class Message(pygame.sprite.Sprite):
    def __init__(self,play,message,delay):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.play = play
        self.display = play.display
        self.core = play.core
        self.message = message
        self.delay = delay
        h = 20+(20*len(self.message))
        w = self.display.screen_width
        self.image = pygame.Surface((w-100,h)).convert_alpha()
        self.image.fill((0,0,0,180))
        cl = water_color
        y = int((self.display.screen_height-h)/2)
        for i in range(len(self.message)):
            te = self.message[i]
            ite = self.display.fonts["Ca18"].render(te,1,cl)
            self.image.blit(ite,(10,10+(20*i)))
        self.rect = self.image.get_rect()
        self.rect.left = 50
        self.rect.top = 50#y
        self.refresh()
 
    def refresh(self,force_refresh=0):
        if self.play.pause_mode != "none":
            return 0
        self.delay += -1
        if self.delay < 0:
            return -1
        return 1

class MyRect(pygame.Rect):
    pass

class Play:
    def __init__(self,display):
        self.display = display
        self.core = display.core

        self.pause_mode = "none"
        self.wait = 0

        self.time = [None,0]

        self.bkgd = None
        self.bkgd_first = 0 #0 if the bkgd should be redrawn
        
        self.layer0_obj = []
        self.layer0_sprite = None
        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None
        self.layer3_obj = []
        self.layer3_sprite = None

        self.player = None
        self.walls = []
        self.bonus = []
        self.exit_properties = None
        self.view_pos = [0,0]

    def init(self):        
        self.layer0_sprite = pygame.sprite.LayeredUpdates()
        self.layer1_sprite = pygame.sprite.LayeredUpdates()
        self.layer2_sprite = pygame.sprite.LayeredUpdates()
        self.layer3_sprite = pygame.sprite.LayeredUpdates()
        DecorBkgd.containers = self.layer0_sprite
        Drop.containers = self.layer1_sprite
        Player.containers = self.layer1_sprite
        Bonus.containers = self.layer1_sprite
        DecorFrgd.containers = self.layer2_sprite
        Gauge.containers = self.layer3_sprite
        Message.containers = self.layer3_sprite
        self.load()

    def load(self):
        self.bkgd = None
        self.bkgd_first = 0 #0 if the bkgd should be redrawn
        for o in self.core.loaded.get("decor_bkgd",[]):
            d = DecorBkgd(self,o[2],o[:2])
            self.layer0_obj.append(d)

        for o in self.core.loaded.get("walls",[]):
            w = pygame.Rect(o)
            self.walls.append(w)

        for o in self.core.loaded.get("bonus",[]):
            b = Bonus(self,o)
            self.bonus.append(b)
            self.layer1_obj.append(b)

        for o in self.core.loaded.get("special",[]):
            self.add_special(o)

        self.exit_properties = self.core.loaded.get("exit") 

        self.player = Player(self)
        for k in self.core.loaded.get("player_prop",{}):
            va = self.core.loaded["player_prop"][k]
            setattr(self.player,k,va)
        self.player.nr = 1
        self.layer1_obj.append(self.player)

        gauge = Gauge(self)
        self.layer3_obj.append(gauge)

        self.view_pos = self.core.loaded.get("view_pos",[0,0])

        tm = self.core.loaded.get("intro_message",[])
        if tm:
            if type(tm) == type(""):
                tm = [tm]
            self.add_message(tm,200)

    def add_special(self,o):
        if o[2] == "bulletexplosion":
            w = pygame.Rect(o[:2]+[80,20])
            self.walls.append(w)
            b = SpecialBE(self,o[:2],o[3],w)
            self.layer0_obj.append(b)
        if o[2] == "hotgirder":
            d = DecorBkgd(self,o[2],o[:2])
            self.layer0_obj.append(d)
            w = MyRect([o[0],o[1]-1,80,20])
            w.fct = self.hurt
            self.walls = [w]+self.walls
        if o[2] == "machine":
            d = DecorBkgd(self,o[2],o[:2])
            d.refresh = self.machine_refresh
            self.layer0_obj.append(d)
    
    def machine_refresh(self0,force_refresh=0):
        self = self0.layer0_obj[-1]
        self.image = self.display.images[self.keyname]
        self.rect = self.image.get_rect()
        self.pos[0] += 8
        if self.pos[0]+190 > self0.player.pos[0]:
            if self0.player.nr == 1:
                self0.player.die()
        self.rect.left = int(round(self.pos[0])+self.play.view_pos[0])
        self.rect.top = int(round(self.pos[1])+self.play.view_pos[1])
        return 1

    def hurt(self):
        if self.player and self.player.nr != -1:
            self.player.size += -0.1
            if self.player.size < 20:
                self.player.die()

    def set_background(self):
        if self.bkgd:
            return
        bg = []
        rw,rh = self.display.screen_width, self.display.screen_height
        self.bkgd = pygame.Surface((rw,rh))
        cl = self.core.loaded.get("background_cl",(0,0,0))
        self.bkgd.fill( cl )

    def create_drop(self,pos,v=None):
        d1 = Drop(self,pos)
        d1.size = 3
        if v is None:
            d1.vx = 0.1*(10-random.randint(0,20))
            d1.vy = 0.5*(10-random.randint(0,20))
        else:
            d1.vx = v[0]
            d1.vy = v[1]
        d2 = Drop(self,pos[:])
        d2.following = d1
        self.layer1_obj.append(d1)
        self.layer1_obj.append(d2)

    def check_key(self,key):
        if self.pause_mode == "none":
            if key == K_UP or key == K_z or key == K_w:
                self.player.move_key(0,-5-int(self.player.size/10))
            elif key == K_DOWN or key == K_s:
                self.player.size += -0.5
                #if self.player.size < 20:
                #    self.player.size = 80
                #print(self.player.size,self.player.pos)
                pass
            elif key == K_LEFT or key == K_a or key == K_q:
                self.player.move_key(-0.1,0)
            elif key == K_RIGHT or key == K_d:
                self.player.move_key(0.1,0)

    def check_events(self,event):
        if event.type == ACTIVEEVENT:
            ##turn on pause if the window is losing focus
            #if self.core.config.pausewhenout:
            #    if event.state == 2 and event.gain == 0:
            #        self.mode = "pause"
            pass
        elif event.type == KEYDOWN:
            key = event.key
            if self.pause_mode == "none":
                if key == K_ESCAPE or key == K_p:
                    self.set_pause()
                elif key == K_r:
                    self.reload_level()
            elif self.pause_mode == "pause":
                if key == K_p:
                    self.pause_mode = "none"
                elif key == K_r:
                    self.pause_mode = "none"
                    self.reload_level()
                elif key == K_ESCAPE:
                    self.pause_mode = "none"
                    self.core.back_to_menu()
            elif self.pause_mode == "end":
                if key == K_ESCAPE:
                    self.pause_mode = "none"
                    self.core.back_to_menu()
                else:
                    self.pause_mode = "none"
                    self.reload_level()
            elif self.pause_mode == "dead":
                if key == K_r:
                    self.pause_mode = "none"
                    self.reload_level()
                elif key == K_ESCAPE:
                    self.pause_mode = "none"
                    self.core.back_to_menu()

    def reload_level(self):
        self.core.new_game()

    def add_message(self,message,delay=1):
        m = Message(self,message,delay)
        self.layer3_obj.append(m)

    def pause_time(self):
        ct = self.time[0]
        st = self.time[1]
        if ct is None:
            return
        dt = time.time()-ct
        self.time[0] = None
        self.time[1] = st+dt

    def set_pause(self):
        self.pause_time()
        t = ["PAUSE"]
        t += ["press 'p' to continue"]
        t += ["press 'ESC' to abandon the game and exit to menu"]
        t += ["press 'r' to restart the level"]
        tm = self.core.loaded.get("intro_message",[])
        if tm:
            t += ["","INTRO MESSAGE:"]
            if type(tm) == type(""):
                tm = [tm]
            t += tm
        self.add_message(t)
        self.pause_mode = "pause"

    def endlevel(self):
        self.display.play_sound("slurp",0.5)
        self.pause_time()
        t = ["Congrats, you finished the level"]
        t += [""]
        t += [" remaining life: %i"%int(round(self.player.size-20))]
        ti = self.get_time()
        tis = time.strftime('%M:%S', time.gmtime(ti))
        if ti > 60*60:
            tis = time.strftime('%H:%M:%S', time.gmtime(ti))
        t += [" time: %s"%tis]
        t += [""]
        t += ["press 'ESC' to exit to menu"]
        t += ["press any other key to continue to next level"]
        self.add_message(t)
        self.core.prepare_nextlevel(self.player.size-20,ti)
        self.pause_mode = "end"

    def die(self):
        self.display.play_sound("pop",0.5)
        self.pause_time()
        t = ["DEAD"]
        t += ["press 'ESC' to exit to menu"]
        t += ["press 'r' to restart the level"]
        self.add_message(t)
        self.pause_mode = "dead"        

    def update(self):
        dirty = []

        if self.pause_mode == "none":
            if self.time[0] == None:
                self.time[0] = time.time()

        #if self.layer1_sprite == None:
        #    return [],0

        if self.bkgd == None:
            self.set_background()
            self.display.screen.blit(self.bkgd, (0,0))


        force_refresh = 0
        if self.player.nr and self.pause_mode == "none":
            x,y = self.player.rect.centerx,self.player.rect.centery
            dx,dy = 0,0
            borderx = 0.25
            bordery = 0.2
            if x < self.display.screen_width*borderx: dx = -0.5*((self.display.screen_width*borderx)-x)
            if x > self.display.screen_width*(1-borderx): dx = -0.5*((self.display.screen_width*(1-borderx))-x)
            if y < self.display.screen_height*bordery: dy = -0.5*((self.display.screen_height*bordery)-y)
            if y > self.display.screen_height*(1-bordery): dy = -0.5*((self.display.screen_height*(1-bordery))-y)
            if dx or dy:
                self.view_pos[0] += -1*dx
                self.view_pos[1] += -1*dy
                force_refresh = 1

        list_layers = [self.layer0_sprite,
                       self.layer1_sprite,
                       self.layer2_sprite,
                       self.layer3_sprite,
        ]
        list_objects = [self.layer0_obj,
                        self.layer1_obj,
                        self.layer2_obj,
                        self.layer3_obj,
        ]

        tokill = []

        for i in range(len(list_layers)):
            for o in list_objects[i][:]:
                r = o.refresh(force_refresh)
                if r==-1:
                    tokill.append(o)
                    list_objects[i].remove(o)

        dirty = []
        for i in range(len(list_layers)):
            if list_layers[i]:
                list_layers[i].clear(self.display.screen, self.bkgd)
        for i in range(len(list_layers)):
            if list_layers[i]:
                dirty += list_layers[i].draw(self.display.screen)
        
        for o in tokill:
            o.kill()

        if self.wait > 0:
            self.wait += -1
            return [],1

        redraw = 0
        if self.bkgd_first == 0:
            self.bkgd_first = 1
            redraw = 1

        return dirty,redraw

    def get_time(self):
        dt = 0
        if self.time[0] != None:
            dt = time.time()-self.time[0]
        tt = dt+self.time[1]
        return tt

    def end(self):
        for o in self.layer0_obj:
            o.kill()
        for o in self.layer1_obj:
            o.kill()
        for o in self.layer2_obj:
            o.kill()
        for o in self.layer3_obj:
            o.kill()

        self.bonus = []
        self.walls = []

        self.bkgd = None
        self.bkgd_first = 0

        self.time = [None,0]

        self.layer0_obj = []
        self.layer0_sprite = None
        self.layer1_obj = []
        self.layer1_sprite = None
        self.layer2_obj = []
        self.layer2_sprite = None
        self.layer3_obj = []
        self.layer3_sprite = None

        if self.player is not None:
            self.player.nr = -1
            self.player.kill()
            self.player = None
        
