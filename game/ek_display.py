#! /usr/bin/env python3

#    This file is part of !!!.
#
#    !!! is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    !!! is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with !!!.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

import os
import random
_thread=None
try:
    import _thread
except ImportError:
    pass

from game.ek_menu import Menu
from game.ek_play import Play
from game.ek_cutscene import Cutscene

class Display:
    def __init__(self):
        self.core = None
        
        self.mode = "play"   #can be "play", "menu"
        self.mode = "menu"

        self.screen_width = 800
        self.screen_height = 600
        
        self.module = {}
        self.screen_winstyle = 0
        self.screen = None
        self.clock = None
        self.music = None

        self.key_pressed = [] #list of keys currently pressed

        self.images = {}
        self.fonts = {}
        self.sounds = {}
        self.sound_channel = None

    def init(self):
        self.module["play"] = Play(self)
        self.module["menu"] = Menu(self)
        self.module["cutscene"] = Cutscene(self)

        if not pygame.image.get_extended():
            raise (SystemExit, "Sorry, extended image module required")

        pygame.init()
        if pygame.mixer and not pygame.mixer.get_init():
            print('Warning, no sound')
            #pygame.mixer = None
        
        pygame.display.gl_set_attribute(pygame.GL_DEPTH_SIZE, 16)
        pygame.display.gl_set_attribute(pygame.GL_STENCIL_SIZE, 1)
        pygame.display.gl_set_attribute(pygame.GL_ALPHA_SIZE, 8)
        self.set_screen(self.core.profile.fullscreen)

        #filename1 = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","font","Nunito-SemiBold.ttf")
        #filename1 = pygame.font.get_default_font()
        #self.fonts['8'] = pygame.font.Font(filename1, 8)
        #self.fonts['12'] = pygame.font.Font(filename1, 12)
        #self.fonts['18'] = pygame.font.Font(filename1, 18)
        #self.fonts['22'] = pygame.font.Font(filename1, 22)

        filename1 = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","font","BubblegumSans-Regular.otf")
        self.fonts['title'] = pygame.font.Font(filename1, 46)
        filename1 = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","font","Cagliostro-Regular.ttf")
        self.fonts['Ca22'] = pygame.font.Font(filename1, 22)
        self.fonts['Ca18'] = pygame.font.Font(filename1, 18)
        self.fonts['Ca12'] = pygame.font.Font(filename1, 12)

        self.clock = pygame.time.Clock()
        
        self.load_px()
        pygame.display.set_icon(self.images["icon"])
        pygame.display.set_caption('Evaporating Kenneth')
        pygame.mouse.set_visible(1)
        try:
            _thread.start_new_thread( self.load_all_sounds, (1, ) )
        except:
            self.load_all_sounds(1)

    def load_all_sounds(self,i):
        self.load_music()
        if i:
            self.change_volume_music()
        self.load_sound()

    def load_music(self):
        if self.core.profile.sound1 == 0:
            return
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                "..","data","music",
                                "Bansheebeat_Stargazer.ogg")
        try:
            self.music = pygame.mixer.Sound(filename)
            self.music.play(-1)
        except:
            print("Failed to play %s"%filename)

    def change_volume_music(self):
        if not self.music:
            self.load_all_sounds(0)
        if self.music:
            factor1 = 0.2*self.core.profile.sound1/6.
            self.music.set_volume(factor1)

    def load_sound(self):
        if not pygame.mixer:
            return
        self.sound_channel = None
        di = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","sound")
        filenames = [ os.path.join(di,f) for f in os.listdir(di) if os.path.isfile(os.path.join(di,f)) and os.path.splitext(f)[1] == ".wav" ]
        for filename in filenames:
            fn = os.path.splitext(os.path.basename(filename))[0] 
            try:
                self.sounds[fn] = pygame.mixer.Sound(filename)
            except:
                print("Failed to load %s"%filename)
        try:
            self.sound_channel = pygame.mixer.find_channel() 
        except:
            print("Failed to initialize the sound channel")

    def play_sound(self,na,volume=1,parallel=0):
        if na not in self.sounds.keys():
            return
        factor1 = self.core.profile.sound2/6.
        try:
            if parallel != 0: 
                self.sounds[na].set_volume(volume*factor1)
                pygame.mixer.find_channel().play(self.sounds[na])
            else:
                self.sounds[na].set_volume(volume*factor1)
                self.sound_channel.queue(self.sounds[na])
        except:
            print("Failed to play %s"%na)

    def random_sound_from_list(self,pattern):
        l = self.sounds.keys()
        s = [x for x in l if x.startswith(pattern)]
        if not s:
            return ""
        return s[random.randint(0,len(s)-1)]

    def set_screen(self,i):
        r = (self.screen_width, self.screen_height)
        bestdepth = pygame.display.mode_ok(r, self.screen_winstyle, 32)
        if i:
            self.screen = pygame.display.set_mode(r, pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.FULLSCREEN, bestdepth)
        else:
            self.screen = pygame.display.set_mode(r, pygame.HWSURFACE | pygame.DOUBLEBUF, bestdepth)
            #self.screen = pygame.display.set_mode(r, pygame.DOUBLEBUF, bestdepth)

    def load_px(self):

        di = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","data","px")

        filenames = [ os.path.join(di,f) for f in os.listdir(di) if os.path.isfile(os.path.join(di,f)) and os.path.splitext(f)[1] == ".png" ]
        for filename in filenames:
            fn = os.path.splitext(os.path.basename(filename))[0] 
            try:
                self.images["%s"%fn] = pygame.image.load(filename).convert_alpha()
            except pygame.error:
                raise (SystemExit, 'Could not load image "%s" %s'%(filename, pygame.get_error()))

        for i in range(1,61):
            w,h = 80-i,80-i
            self.images["player_r_%02i"%(w)] = pygame.Surface((80,80)).convert_alpha()
            self.images["player_r_%02i"%(w)].fill ( (0,0,0,0) )
            new_image = pygame.transform.smoothscale(self.images["player_r_80"], (w,h))
            self.images["player_r_%02i"%(w)].blit(new_image, (int(0.5*i),i))
            self.images["player_r_%02i"%(w)].blit(self.images["player_r_eyes"], (int(-0.3*i),int(0.5*i)))
            self.images["player_l_%02i"%(w)] = pygame.transform.flip(self.images["player_r_%02i"%(w)],True,False)
        self.images["player_r_80"].blit(self.images["player_r_eyes"], (0,0))
        self.images["player_l_80"] = pygame.transform.flip(self.images["player_r_80"],True,False)
        self.images["player_l_eyes"] = pygame.transform.flip(self.images["player_r_eyes"],True,False)

        #for i in range(1,11):
        #    w,h = 16-i,16-i
        #    self.images["drop_%02i"%(10-i)] = pygame.transform.smoothscale(self.images["drop_10"], (w,h))


    def check_key(self):
        cm = self.module[self.mode]
        for key in self.key_pressed:
            cm.check_key(key)

    def mousepress(self,button,xy):
        pass
        #if button[0] == 1:
        #    print("button")

    def check_events(self):
        cm = self.module[self.mode]
        for event in pygame.event.get():
            if event.type == QUIT:
                self.core.play = 0
            elif event.type == KEYDOWN:
                if event.key not in self.key_pressed:
                    self.key_pressed.append(event.key)
            elif event.type == KEYUP:
                if event.key in self.key_pressed:
                    self.key_pressed.remove(event.key)
            elif event.type == MOUSEBUTTONDOWN:
                self.mousepress(pygame.mouse.get_pressed(),pygame.mouse.get_pos())
            cm.check_events(event)
        self.check_key()

    def redraw(self):
        cm = self.module[self.mode]
        self.screen.blit(cm.bkgd, (0,0))
        cm.bkgd_first = 0        

    def update(self):
        self.check_events()
        cm = self.module[self.mode]
        dirty,redraw = cm.update()
        for l in dirty:
            pygame.display.update(l)
        if redraw:
            pygame.display.update()
        self.clock.tick(self.core.fps)

    def end(self):
        pass
