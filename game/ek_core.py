#! /usr/bin/env python3

#    This file is part of Evaporating Kenneth.
#
#    Evaporating Kenneth is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Evaporating Kenneth is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Evaporating Kenneth.  If not, see <http://www.gnu.org/licenses/>.

import random
import os
from game.ek_profile import Profile

class Core:
    def __init__(self):
        self.display = None
        self.play = 1        # 0:quit, 1:play
        self.time = 0        # time in iteration of the while loop
        self.fps = 40        # fps used in display -> 1 iteration = 1./fps seconds

        self.loaded = {}

        self.profile = Profile(self)
        self.profile.load()

    def load(self):
        #initialize Display
        self.display.init()
        if self.display.mode == "play":
            self.load_play()
        elif self.display.mode == "menu":
            self.load_menu()
        else:
            cm = self.display.module[self.display.mode]
            cm.init()
        #main loop
        self.run()

    def load_menu(self):
        cm = self.display.module["menu"]
        self.display.mode = "menu"
        cm.end()
        cm.init()

    def new_game(self, level=None):
        mode = self.display.mode
        cm = self.display.module[mode]
        cm.end()
        if level is None:
            level = self.profile.lastlevel
        if level == "!menu":
            self.profile.lastlevel = "cutscene12"
            self.back_to_menu()
        else:
            self.load_level(level)
            if self.loaded.get("cutscene"):
                self.display.mode = "cutscene"
                self.load_cutscene()
            else:
                self.display.mode = "play"
                self.load_play()

    def back_to_menu(self):
        mode = self.display.mode
        cm = self.display.module[mode]
        cm.end()
        self.load_menu()

    def load_cutscene(self):
        cm = self.display.module["cutscene"]
        cm.init()

    def load_play(self):
        cm = self.display.module["play"]
        cm.init()

    def load_level(self,level_name):
        self.loaded = {}
        if level_name == "":
            level_name = "test"
        self.loaded["levelname"] = level_name
        filename = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..","level","%s.py"%level_name)
        if os.path.exists(filename):
            exec(open(filename).read())
        else:
            print('level file not found: %s'%level_name)

    def prepare_nextlevel(self,timeplay,health):
        currentlevel = self.loaded["levelname"]
        # 1) set the next level
        self.profile.lastlevel = self.loaded.get("exit",[None,None,""])[2]
        # 2) save the highscore
        if timeplay is not None:
            self.profile.add_highscore(currentlevel,timeplay,health)
        self.profile.save()

    def run(self):
        while self.play:
            self.display.update()
            if self.display.mode != "pause":
                self.time += 1
                self.update()
        self.display.end()

    def update(self):
        pass


