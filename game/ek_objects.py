#! /usr/bin/env python3

#    This file is part of Evaporating_Kenneth.
#
#    Evaporating_Kenneth is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Evaporating_Kenneth is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Evaporating_Kenneth.  If not, see <http://www.gnu.org/licenses/>.

#import basic pygame modules
try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("pygame 1.9.3 or higher is required")

import os
import random
import math
import time

water_color = [100,150,238]

class Drop(pygame.sprite.Sprite):
    def __init__(self,play,pos):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.play = play
        self.core = play.core
        self.display = play.display
        self.image = pygame.Surface((10,10)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.rect = self.image.get_rect()
        self.lifetime = 0
        self.transparency = 1
        self.vx = 0
        self.vy = 0
        self.pos = pos
        self.size = 6
        self.following = None
        self.refresh()
 
    def refresh(self,force_refresh=0):
        if self.play.pause_mode in ["pause"]:
            return 0
        maximum = 50
        self.lifetime += 1
        if self.lifetime > maximum:
            return -1
        size = self.size
        if self.following is not None:
            size = self.following.size-1

        self.image = pygame.Surface((10,10)).convert_alpha()
        self.image.fill((0,0,0,0))

        pygame.draw.circle(self.image, water_color+[250], [5,5], size)

        #self.image = self.display.images["drop_%02i"%int(size)].copy()
        alpha = self.transparency*255*(maximum-self.lifetime)/maximum
        self.image.fill((255, 255, 255, alpha), None, pygame.BLEND_RGBA_MULT)
        self.rect = self.image.get_rect()
        w,h = self.rect.width,self.rect.height
        if self.following is None:
            self.pos[0] += self.vx
            self.pos[1] += (self.lifetime*0.2)+self.vy
        else:
            dx,dy = 0,0
            if self.following.lifetime == self.lifetime:
                dx = self.following.vx
                dy = (self.lifetime-1)*0.2
            self.pos[0] = self.following.pos[0]-dx
            self.pos[1] = self.following.pos[1]-dy
        self.rect.left = int(round(self.pos[0]-(w/2))+self.play.view_pos[0])
        self.rect.top = int(round(self.pos[1]-(h/2))+self.play.view_pos[1])
        return 1

class PlayerPart:
    def __init__(self,player):
        self.player = player
        self.pos = []
        r = (random.random()-0.5)*2
        a = random.random()*2*math.pi
        self.momentum = [r*math.cos(a),r*math.sin(a)]

    def update(self,collision):
        size = (self.player.size/2)
        cr = 6+int(self.player.size/10)
        size = size-cr+0.5
        mx,my = self.momentum[0],self.momentum[1]
        mx += random.random()-0.5
        my += random.random()-0.5
        x,y = self.pos[0],self.pos[1]
        d = math.sqrt(x**2 + y**2)
        if y > 0: d += ((y/size)**2)
        if y < 0: d -= 0.005*(x**2)
        if d > size:
            self.momentum = [-1*x/d,-1*y/d]
        self.pos = [x+mx,y+my]

        # we can check collision here, to have a "part" level collision
        # but for now, let's do simple
        if 0:
            #1) lateral motion collision
            # take a smaller rectangle on the right and on the left
            center = [50,self.player.size/2]
            rx = x+mx+center[0]-int(cr/2)+self.player.pos[0]
            ry = y+my+center[1]-int(cr/2)+self.player.pos[1]
            R = pygame.Rect(rx,ry,cr,cr)
            cw = [self.player.play.walls[i] for i in collision]
            col = R.collidelist(cw)
            if col != -1:
                print(collision,col,cw)
                #self.player.pos[1] = cw[col].top
                #self.player.momentum[1] = 0

class Player(pygame.sprite.Sprite):
    def __init__(self,play):
        pygame.sprite.Sprite.__init__(self, self.containers)
        self.play = play
        self.core = play.core
        self.display = play.display
        self.size = 80
        self.image = pygame.Surface((100,100)).convert_alpha()
        self.falling = 0
        self.ori = "r"
        self.parts = []
        self.rect = self.image.get_rect()
        self.pos = [0,0]
        self.nr = 1
        self.momentum = [0,0]
        self.key_momentum = [0,0]
        self.max_y = None
        #self.refresh()
 
    def draw_parts(self):
        # check collision
        #collision = self.rect.collidelistall(self.play.walls)
        #to complicated for now, maybe later
        collision = []

        while len(self.parts)<self.size:
            part = PlayerPart(self)
            r = (random.random()-0.5)*self.size
            a = random.random()*2*math.pi
            part.pos = [r*math.cos(a),r*math.sin(a)]
            self.parts.append(part)
        while len(self.parts)>self.size:
            self.parts.pop()
        self.image = pygame.Surface((100,100)).convert_alpha()
        self.image.fill((0,0,0,0))
        new_image = pygame.Surface((100,100)).convert_alpha()
        new_image.fill((0,0,0,0))

        #center = [60,self.size/2]
        ## pygame.draw.circle(new_image, water_color+[150], [center[0],80+int(self.size/2)], int(self.size*0.75))
        #dx = self.size*0.38
        #pygame.draw.ellipse(new_image, water_color+[150], pygame.Rect(int(center[0]-dx),80-int(self.size/2),int(dx*2),self.size))
        #pygame.draw.rect(new_image, (0,0,0,0), pygame.Rect(0,70,80,80))
        ## pygame.draw.line(new_image, (0,0,0,150), [int(center[0]-(self.size/3)),80-10], [int(center[0]+(self.size/3)),80-10])

        #sr = (80-self.size)*0.4
        #width1 = int((40-sr)/2)
        #width2 = int((40-sr)/3)
        #pygame.draw.rect(new_image, (100,0,0,100), pygame.Rect(10+int(sr),25+int(sr*2)+width2,width1,63-int(sr*2)-width2))
        #pygame.draw.rect(new_image, (100,50,0,100), pygame.Rect(10+int(sr)+width1,25+int(sr*2),width1,63-int(sr*2)))
        #pygame.draw.rect(new_image, (0,100,0,100), pygame.Rect(10+int(sr)+width1,25+int(sr*2),width1*2,20))

        # pygame.draw.rect(new_image, (0,100,0,100), pygame.Rect(50,20+int(sr*2),40-int(sr),68-int(sr*2)))

        center = [50,self.size/2]
        i = 0
        for p in self.parts:
            i += 1
            p.update(collision)
            x = p.pos[0]+center[0]
            y = 100-(p.pos[1]+center[1])
            pygame.draw.circle(new_image, water_color+[150], [int(x),int(y)], 6+int(self.size/10))
            if i%10 == 0:
                self.image.blit(new_image, (0,0))
                new_image = pygame.Surface((100,100)).convert_alpha()
                new_image.fill((0,0,0,0))
        if i%10 != 0:
            self.image.blit(new_image, (0,0))
        i = 80-self.size
        ori = 1
        if self.ori == "r":
            ori = -1
        self.image.blit(self.display.images["player_%s_eyes"%self.ori], (10+int(ori*0.3*i),20+int(0.5*i)))


    def refresh(self,force_refresh=0):
        if self.play.pause_mode in ["pause", "end"]:
            return 0
        if self.nr == 0:
            return 0
        if self.nr == -1:
            self.nr = 0
            return -1
        if self.size < 20:
            self.size = 20
        if self.size > 80:
            self.size = 80
        self.draw_parts()
        #self.image = self.display.images["player_%s_eyes"%(self.ori)]
        self.rect = self.image.get_rect()
        w,h = self.rect.width,self.rect.height
        factor = 0.95
        if self.falling:
            factor = 1
        self.momentum = self.dilute(self.momentum,factor)
        self.key_momentum = self.dilute(self.key_momentum,0.9)
        self.pos[0] += self.momentum[0]
        self.size += -0.01*abs(self.momentum[0])
        if self.size < 20 or self.size > 80:
            self.die()
        self.pos[1] += self.momentum[1]
        self.rect.left = int(round(self.pos[0]-(w/2))+self.play.view_pos[0])
        self.rect.top = int(round(self.pos[1]-h)+self.play.view_pos[1])
        self.check_gravity()
        self.check_collision("l")
        self.check_collision("r")
        self.check_collision("u")
        self.check_bonus()
        self.check_exit()
        if self.max_y is not None and self.pos[1] > self.max_y:
            self.die()
        if random.randint(0,10) == 0:
            pos = self.pos[:]
            pos[0] += (random.random()-0.5)*self.size
            pos[1] += -10#20+self.size
            v = [self.momentum[0]*0.2,self.momentum[1]*0.2]
            self.play.create_drop(pos,v)
        return 1

    def check_exit(self):
        if self.play.exit_properties is None:
            return
        w,h = self.rect.width,self.rect.height
        x1 = self.pos[0]
        y1 = self.pos[1]-20#+h-20-(self.size/2)
        x2,y2 = self.play.exit_properties[0]+20,self.play.exit_properties[1]
        d = (x1-x2)**2 + (y1-y2)**2 
        if d < 400:
            self.play.endlevel()

    def check_bonus(self):
        w,h = self.rect.width,self.rect.height
        x = self.rect.left
        y = self.rect.top
        sr = (80-self.size)*0.4
        width1 = int((40-sr)/2)
        width2 = int((40-sr)/3)
        x1 = x+10+int(sr)
        y1 = y+25+int(sr*2)
        h1 = h-25-int(sr*2)
        p1 = pygame.Rect(x1+width1,y1,width1*2,int(h1))

        for b in self.play.bonus[:]:
            col = b.rect.colliderect(p1)
            if col:
                b.nr = -1
                self.display.play_sound("drop",0.5)
                self.size += 10
                self.play.bonus.remove(b)

    def die(self):
        for i in range(10+int(self.size)):
            pos = self.pos[:]
            pos[0] += (random.random()-0.5)*self.size
            pos[1] += -10+((random.random()-0.5)*self.size)
            r = 1+random.random()
            a = random.random()*2*math.pi
            v = [r*math.cos(a),r*math.sin(a)]
            self.play.create_drop(pos,v)
        self.image = pygame.Surface((0,0)).convert_alpha()
        self.image.fill((0,0,0,0))
        self.momentum = [0,0]
        self.nr = 0
        self.play.die()

    def dilute(self,v,speed):
        v[0] *= speed
        if abs(v[0]) < 0.05:
            v[0] = 0
        return v

    def move_key(self,x,y):
        if x < 0:
            self.ori = "l"
        if x > 0:
            self.ori = "r"
        if self.falling:
            return
        if self.nr == 0:
            return
        self.move(x,y)

    def move(self,x,y):
        self.key_momentum[0] += x
        self.momentum[0] = self.momentum[0]+self.key_momentum[0]
        self.momentum[0] = min(10,self.momentum[0])
        self.momentum[0] = max(-10,self.momentum[0])
        #self.key_momentum[1] += y
        self.momentum[1] = self.momentum[1]+y #self.key_momentum[1]
        self.momentum[1] = min(100,self.momentum[1])
        self.momentum[1] = max(-100,self.momentum[1])
        if x < 0:
            self.ori = "l"
        if x > 0:
            self.ori = "r"

    def check_collision(self,dire):
        # simple check of collision with walls
        rects = []
        w,h = self.rect.width,self.rect.height
        x = self.pos[0]-(w/2)
        y = self.pos[1]-h
        sr = (80-self.size)*0.4
        width1 = int((40-sr)/2)
        width2 = int((40-sr)/3)
        if dire == "l":
            #collision to the left
            x1 = x+10+int(sr)
            y1 = y+25+int(sr*2)
            p1 = pygame.Rect(x1,y1+width2,width1,63-int(sr*2)-width2)
            p2 = pygame.Rect(x1+width1,y1+1,width1,63-int(sr*2)-1)
            rects.append(p1)
            rects.append(p2)
        elif dire == "r":
            x1 = x+50
            y1 = y+25+int(sr*2)
            p1 = pygame.Rect(x1,y1+width2,width1,63-int(sr*2)-width2)
            p2 = pygame.Rect(x1+width1,y1+1,width1,63-int(sr*2)-1)
            rects.append(p1)
            rects.append(p2)
        elif dire == "u":
            #collision from the top
            x1 = x+10+int(sr)
            y1 = y+25+int(sr*2)
            p1 = pygame.Rect(x1+width1,y1-2,width1*2,int(self.size*0.75))
            rects.append(p1)
        col = -1
        for r in rects:
            col = r.collidelist(self.play.walls)
            if col != -1:
                break
        if col != -1:
            gpos = self.pos[:]
            v = None
            if dire == "r":
                self.momentum[0] = 0
                self.pos[0] += -2
                gpos[1] += -20-(self.size*0.5*random.random())
                gpos[0] += -0.4*self.size
                v = [-1,0]#random.random()-0.5]
            elif dire == "l":
                self.momentum[0] = 0                
                self.pos[0] += 2
                gpos[1] += -20-(self.size*0.5*random.random())
                gpos[0] += 0.4*self.size
                v = [1,0]#random.random()-0.5]
            elif dire == "u":
                self.momentum[1] = 0
                self.pos[1] += 2
                gpos[0] += (random.random()-0.5)*self.size
                gpos[1] += -20
                v = [0,1]
            self.play.create_drop(gpos,v)

    def check_gravity(self):
        p1 = pygame.Rect(self.pos[0]-int(self.size/3),self.pos[1]-10,1,1)
        p2 = pygame.Rect(self.pos[0],self.pos[1]-10,1,1)
        p3 = pygame.Rect(self.pos[0]+int(self.size/3),self.pos[1]-10,1,1)
        col1 = p2.collidelist(self.play.walls)
        col2 = p1.collidelist(self.play.walls)
        col3 = p3.collidelist(self.play.walls)
        col = [col1,col2,col3]
        if col == [-1,-1,-1]:
            if self.falling == 0:
                # reduce the resolution to allow more easily reproducible jump
                mx = int(abs(self.momentum[0]))
                if self.momentum[0] < 0:
                    self.momentum[0] = -1*mx
                else:
                    self.momentum[0] = mx
            self.falling = 1
        else:
            self.falling = 0
            for i in col:
                if i == -1:
                    continue
                coli = i
                if "fct" in dir(self.play.walls[i]):
                    self.play.walls[i].fct()
                    break
            self.size += -0.005*(self.momentum[1]**2)
            self.momentum[1] = 0
            self.pos[1] = self.play.walls[coli].top+10
        if self.falling:
            self.move(0,0.5)

