#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
self.loaded["exit"] = [None,None,"level10"]

cm.scene_list = []
w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"There is a machine to clean up the air vent.","p":[120,115]})
sc.append({"?":"text","text":"My men have just sent it. This time, Kenneth will not escape.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":300,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Hurray, I guess.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Oh don't act so self-righteous.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Son, we live in a world that has walls, and those walls have to be","p":[120,115]})
sc.append({"?":"text","text":"guarded by men with mops. Who's gonna do it? You?","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"I've already heard that somewhere.","p":[120,115]})
cm.scene_list.append(sc)


#sc = []
#sc.append({"?":"earth","p":[w/2,h/2]})
#cm.scene_list.append(sc)
