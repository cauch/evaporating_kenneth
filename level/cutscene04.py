#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
self.loaded["exit"] = [None,None,"level04"]

cm.scene_list = []
w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Good news, my men are shooting the vent.","p":[120,115]})
sc.append({"?":"text","text":"It will not escape.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Jeez, all this violence.","p":[120,115]})
sc.append({"?":"text","text":"I should have stayed at Nasa doing fake exploration*.","p":[120,145]})
sc.append ({"?":"text","text":"*: cf. bing the robot, pyweek 24","p":[100,h-80],"f":"Ca12"})
cm.scene_list.append(sc)
