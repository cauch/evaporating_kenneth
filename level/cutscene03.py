#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
self.loaded["exit"] = [None,None,"level03"]

cm.scene_list = []
w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"My men have located it.","p":[120,115]})
sc.append({"?":"text","text":"It is in the air ventilation system. But out of reach.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Poor Kenneth, all lost in the air vent.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"What the hell is a kenneth?","p":[120,115]})
sc.append({"?":"text","text":"AQ712 is a military experiment, not your mother's puppy dog.","p":[120,145]})
cm.scene_list.append(sc)
