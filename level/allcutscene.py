#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level01"]

cm = self.display.module["cutscene"]
cm.scene_list = []

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"This is a disaster! How can it have escape?!","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Well, it is a living liquid form.","p":[120,115]})
sc.append({"?":"text","text":"Liquids are notoriously tricky to contain.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"A cup, son! You should have used a goddamn cup.","p":[120,115]})
cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level02"]

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"We have no idea where it is.","p":[120,115]})
sc.append({"?":"text","text":"Should I remind you what will happen to our bottocks if it escapes?","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Hopefully, it will lose all its liquid when moving.","p":[120,115]})
sc.append({"?":"text","text":"Without liquid, it will die.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Are you saying that the problem will solve itself?","p":[120,115]})
sc.append({"?":"text","text":"For once you did not make my day worse.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Well ...unless it finds drop of humidity and uses it to regenerate.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Okay. I retrack my last statement.","p":[120,115]})
cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level03"]

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"My men have located it.","p":[120,115]})
sc.append({"?":"text","text":"It is in the air ventilation system. But out of reach.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Poor Kenneth, all lost in the air vent.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"What the hell is a kenneth?","p":[120,115]})
sc.append({"?":"text","text":"AQ712 is a military experiment, not your mother's puppy dog.","p":[120,145]})
cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level04"]

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Good news, my men are shooting the vent.","p":[120,115]})
sc.append({"?":"text","text":"It will not escape.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Jeez, all this violence.","p":[120,115]})
sc.append({"?":"text","text":"I should have stayed at Nasa doing fake exploration*.","p":[120,145]})
sc.append ({"?":"text","text":"*: cf. bing the robot, pyweek 24","p":[100,h-80],"f":"Ca12"})
cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level05"]

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Well. This did not work.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"But we need to stop Kenneth.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"I mean, AQ712.","p":[120,115]})
cm.scene_list.append(sc)

#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level06"]

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Why is it called Kenneth anyway?","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"He looks a little bit like Kenneth Branagh.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"A little bit.","p":[120,115]})
cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level07"]

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Ok. Here is another idea.","p":[120,115]})
sc.append({"?":"text","text":"Let's use flame-throwers to heat up the vent.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"It's your answer for everything.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":300,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"No it's not.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"You proposed flame-throwers last week.","p":[120,115]})
sc.append({"?":"text","text":"It was about the broken coffee machine.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":300,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"So? One time!","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"And two weeks ago, about the disposal of the old office equippment.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":300,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"It could have worked!","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":300,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"It did not.","p":[120,115]})
cm.scene_list.append(sc)

#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level08"]

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Well, it did not work.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":300,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"'told you so.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":300,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Shut up.","p":[120,115]})
cm.scene_list.append(sc)

#sc = []
#sc.append({"?":"earth","p":[w/2,h/2]})
#cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level09"]

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"This is a real disaster.","p":[120,115]})
sc.append({"?":"text","text":"We should find a way to stop it.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":300,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"If you say so.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Do you even realize the gravity of the situation, civilian?","p":[120,115]})
sc.append({"?":"text","text":"We need to stop it.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"It's just a little drop.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"It's a secret military project!","p":[120,115]})
sc.append({"?":"text","text":"It's super dangerous and escaping.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Haven't you seen any movie?","p":[120,115]})
sc.append({"?":"text","text":"Each time it happens, there are plenty of deaths.","p":[120,145]})
cm.scene_list.append(sc)

#sc = []
#sc.append({"?":"earth","p":[w/2,h/2]})
#cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level10"]

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"There is a machine to clean up the air vent.","p":[120,115]})
sc.append({"?":"text","text":"My men have just sent it. This time, Kenneth will not escape.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":300,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Hurray, I guess.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Oh don't act so self-righteous.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Son, we live in a world that has walls, and those walls have to be","p":[120,115]})
sc.append({"?":"text","text":"guarded by men with mops. Who's gonna do it? You?","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"I've already heard that somewhere.","p":[120,115]})
cm.scene_list.append(sc)


#sc = []
#sc.append({"?":"earth","p":[w/2,h/2]})
#cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"level11"]

w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Damned. This has failed too.","p":[120,115]})
sc.append({"?":"text","text":"And AQ712 has almost reached the exit of the vent.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Well, I guess he escaped.","p":[120,115]})
sc.append({"?":"text","text":"Maybe it's better like that for the little fellow.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Nothing else to do, any way.","p":[120,115]})
sc.append({"?":"text","text":"Another military project that will end up in the bottom of the ocean.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"What do you mean?","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Well, the air vent leads to the sea coast.","p":[120,115]})
sc.append({"?":"text","text":"Kenneth will drop into the sea.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Wait! WHAT!","p":[120,115]})
cm.scene_list.append(sc)


#sc = []
#sc.append({"?":"earth","p":[w/2,h/2]})
#cm.scene_list.append(sc)
#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
self.loaded["exit"] = [None,None,"!menu"]

w,h = self.display.screen_width,self.display.screen_height

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Into the sea! But it means it will merge with it.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"text","text":"PLOOOF","p":[220,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Oh no.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"earth","p":[w/2,h/2]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"earth","p":[w/2,h/2]})
sc.append({"?":"text","text":"You finished the game.","p":[120,h-200]})
sc.append({"?":"text","text":"Thank you for playing.","p":[120,h-180]})
sc.append({"?":"text","text":"(go to 'options' to reset the game if you want to replay)","f":"Ca12","p":[120,h-150]})
cm.scene_list.append(sc)
