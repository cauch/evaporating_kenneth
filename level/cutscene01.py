#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
self.loaded["exit"] = [None,None,"level01"]

cm.scene_list = []
w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"This is a disaster! How can it have escape?!","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Well, it is a living liquid form.","p":[120,115]})
sc.append({"?":"text","text":"Liquids are notoriously tricky to contain.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"A cup, son! You should have used a goddamn cup.","p":[120,115]})
cm.scene_list.append(sc)
