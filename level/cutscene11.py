#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
self.loaded["exit"] = [None,None,"level11"]

cm.scene_list = []
w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Damned. This has failed too.","p":[120,115]})
sc.append({"?":"text","text":"And AQ712 has almost reached the exit of the vent.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Well, I guess he escaped.","p":[120,115]})
sc.append({"?":"text","text":"Maybe it's better like that for the little fellow.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Nothing else to do, any way.","p":[120,115]})
sc.append({"?":"text","text":"Another military project that will end up in the bottom of the ocean.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"What do you mean?","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":600,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Well, the air vent leads to the sea coast.","p":[120,115]})
sc.append({"?":"text","text":"Kenneth will drop into the sea.","p":[120,145]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":400,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Wait! WHAT!","p":[120,115]})
cm.scene_list.append(sc)


#sc = []
#sc.append({"?":"earth","p":[w/2,h/2]})
#cm.scene_list.append(sc)
