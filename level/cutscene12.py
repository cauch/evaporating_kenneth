#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
self.loaded["exit"] = [None,None,"!menu"]

cm.scene_list = []
w,h = self.display.screen_width,self.display.screen_height

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Into the sea! But it means it will merge with it.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"text","text":"PLOOOF","p":[220,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"Oh no.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"earth","p":[w/2,h/2]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"earth","p":[w/2,h/2]})
sc.append({"?":"text","text":"You finished the game.","p":[120,h-200]})
sc.append({"?":"text","text":"Thank you for playing.","p":[120,h-180]})
#sc.append({"?":"text","text":"(go to 'options' to reset the game if you want to replay)","f":"Ca12","p":[120,h-150]})
cm.scene_list.append(sc)
