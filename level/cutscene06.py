#the important variables are:
#cutscene: non null
self.loaded["cutscene"] = 1
cm = self.display.module["cutscene"]
self.loaded["exit"] = [None,None,"level06"]

cm.scene_list = []
w,h = self.display.screen_width,self.display.screen_height
sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":-50,"p":[100,95]})
sc.append({"?":"text","text":"Why is it called Kenneth anyway?","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"He looks a little bit like Kenneth Branagh.","p":[120,115]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
cm.scene_list.append(sc)

sc = []
sc.append({"?":"img","n":"general","p":[160,h-247]})
sc.append({"?":"img","n":"scientist","p":[w-340,h-220]})
sc.append({"?":"bubble","w":500,"h":100,"lh":50,"ld":50,"p":[100,95]})
sc.append({"?":"text","text":"A little bit.","p":[120,115]})
cm.scene_list.append(sc)
