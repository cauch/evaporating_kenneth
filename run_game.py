#! /usr/bin/env python3

#    This file is part of Evaporating Kenneth.
#
#    Evaporating Kenneth is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Evaporating Kenneth is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Evaporating Kenneth.  If not, see <http://www.gnu.org/licenses/>.

import sys

if len(sys.argv) > 1 and sys.argv[1] == "creator":
    from game import ek_creator
    ek_creator.main()  
    exit(0)

from game import main
main.main()
